import { makeAutoObservable } from "mobx";

//STORE FOR AUTHENTICATION
class AuthStore {
  authenticated = false;

  constructor() {
    makeAutoObservable(this);
  }

  isAuthenticated() {
    return this.authenticated;
  }

  changeAuthenticationTrue() {
    this.authenticated = true;
  }
  changeAuthenticationFalse() {
    this.authenticated = false;
  }
}

export const AuthData = new AuthStore();

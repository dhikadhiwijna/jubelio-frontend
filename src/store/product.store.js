import { makeAutoObservable } from "mobx";

//STORE FOR PRODUCT BY ID
class ProductStore {
  id = 0;
  name = "";
  sku = "";
  image = "";
  price = 0;
  description = "";

  constructor() {
    makeAutoObservable(this);
  }

  setDataProductStore = (data) => {
    this.id = data.id;
    this.name = data.name;
    this.sku = data.sku;
    this.image = data.image;
    this.price = data.price;
    this.description = data.description;
  };

  setName = (name) => {
    this.name = name;
  };

  setSku = (sku) => {
    this.sku = sku;
  };
  setimage = (image) => {
    this.image = image;
  };
  setPrice = (price) => {
    this.price = price;
  };
  setDescription = (description) => {
    this.description = description;
  };
  setImage = (image) => {
    this.image = image;
  };

  getDataProductStore = () => {
    return {
      id: this.id,
      name: this.name,
      sku: this.sku,
      image: this.image,
      price: this.price,
      description: this.description,
    };
  };
}

export const productStore = new ProductStore();

import { observer } from "mobx-react";
import { useState, useEffect } from "react";
// import Navbar from "./component/Navbar";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Navbar from "./component/Navbar";
import { AuthData } from "./store/auth.store";
import { getWithExpiry } from "./helper/tokenExpire";
import Home from "./view/Home";
import SignIn from "./view/SignIn";
import SignUp from "./view/SignUp";
import Products from "./view/Product";
import ProductDetail from "./view/ProductDetail";
import CreateProduct from "./view/CreateProduct";
import User from "./view/User";

const App = () => {
  //DECLARE STATE
  const [open, setOpen] = useState(false);
  const authenticated = AuthData.isAuthenticated();

  //DECLARE FUNCTION FOR CHECKING TOKEN
  useEffect(() => {
    let token = getWithExpiry("token");
    if (token) {
      AuthData.changeAuthenticationTrue();
    }
  }, []);

  return (
    <div
      className={`flex duration-300 bg-hero-pattern h-full ${
        authenticated && "pl-10 md:pl-20"
      }`}
    >
      <BrowserRouter>
        <Navbar open={open} setOpen={setOpen} />
        <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/signin" element={<SignIn />} />
          <Route path="/signup" element={<SignUp />} />
          <Route path="/user" element={<User />} />
          <Route path="/product" element={<Products />} />
          <Route path="/product/create" element={<CreateProduct />} />
          <Route path="/product/:id" element={<ProductDetail />} />
        </Routes>
      </BrowserRouter>
    </div>
  );
};

export default observer(App);

import { FaDatabase, FaDoorOpen, FaUser } from "react-icons/fa";

const Menu = [
  {
    title: "User",
    icon: <FaUser />,
  },
  {
    title: "Products",
    icon: <FaDatabase />,
  },
  {
    title: "Logout",
    icon: <FaDoorOpen />,
  },
];

export default Menu;

import React from "react";
import { useNavigate } from "react-router";
import { URL } from "../config/url";
import { getWithExpiry } from "../helper/tokenExpire";
import { FaInfo } from "react-icons/fa";

const DeleteModal = ({ shown, close, id }) => {
  //DECLARE STATE
  const navigate = useNavigate();

  //HANDLE DELETE PRODUCT
  const onClickDelete = async (id) => {
    try {
      const response = await fetch(`${URL}/products/delete/${id}`, {
        method: "DELETE",
        headers: {
          token: getWithExpiry("token"),
        },
      });
      const result = await response.json();
      if (result) {
        navigate("../product");
        close();
      }
      console.log(result);
    } catch (error) {
      console.log("Error catch onSubmit: ", error);
      close();
    }
  };

  return (
    shown && (
      <div>
        <div
          className="fixed inset-0 overflow-y-auto flex justify-center items-center z-50 bg-black bg-opacity-50"
          onClick={() => {
            // close modal when outside of modal is clicked
            close();
          }}
        >
          <div className="bg-white px-4 pt-5 pb-4 sm:p-6 sm:pb-4 rounded-lg">
            <div className="">
              <div className="flex-shrink-0 flex mx-auto items-center justify-center h-12 w-12 rounded-full bg-red-100 ml-3 mb-3 sm:h-10 sm:w-10">
                <FaInfo className="h-6 w-6 text-red-600" />
              </div>
              <div className="mt-3 text-center sm:mt-0 sm:ml-4 sm:text-left">
                <div>DELETE PRODUCT</div>
                <div className="mt-2">
                  <p className="text-sm text-gray-500">
                    Are you sure you want to delete this product?
                  </p>
                </div>
              </div>
            </div>
            <div className="bg-gray-50 py-3 sm:px-6 sm:flex sm:flex-row-reverse">
              <button
                onClick={() => {
                  onClickDelete(id);
                }}
                type="button"
                className="w-full inline-flex justify-center rounded-md border border-transparent shadow-sm px-4 py-2 bg-red-600 text-base font-medium text-white hover:bg-red-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-red-500 sm:ml-3 sm:w-auto sm:text-sm"
                // onClick={() => setOpen(false)}
              >
                Delete
              </button>
              <button
                onClick={() => close()}
                type="button"
                className="mt-3 w-full inline-flex justify-center rounded-md border border-gray-300 shadow-sm px-4 py-2 bg-white text-base font-medium text-gray-700 hover:bg-gray-50 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:mt-0 sm:ml-3 sm:w-auto sm:text-sm"
                // onClick={() => setOpen(false)}
              >
                Cancel
              </button>
            </div>
          </div>
        </div>
      </div>
    )
  );
};

export default DeleteModal;

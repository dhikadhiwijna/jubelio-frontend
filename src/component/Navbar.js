import { observer } from "mobx-react";
import React from "react";
import { FaAngleLeft } from "react-icons/fa";
import { useNavigate } from "react-router";
import logo from "../assets/logo.png";
import Menu from "../data/menu";
import { AuthData } from "../store/auth.store";

const Navbar = ({ open, setOpen }) => {
  //DECLARE STATE
  const navigate = useNavigate();
  const auth = AuthData.isAuthenticated();

  //ON CLICK NAVBAR
  const onClickNavbar = (index) => {
    setOpen(false);
    if (index === 0) {
      navigate("/user");
    }
    if (index === 1) {
      navigate("/product");
    }
    if (index === 2) {
      AuthData.changeAuthenticationFalse();
      localStorage.removeItem("token");
      navigate("/");
    }
  };

  return (
    <>
      {auth && (
        <div
          className={`${
            open ? "w-30 md:w-72" : "w-10 md:w-20"
          } h-screen bg-dark-purple duration-300 container fixed top-0 left-0 z-10`}
        >
          <FaAngleLeft
            className={`absolute cursor-pointer -right-3 top-9 w-7 h-7 border-2 border-dark-purple rounded-full bg-white ${
              !open && "rotate-180"
            }`}
            onClick={() => setOpen(!open)}
          />
          <div
            onClick={() => {
              navigate("/");
            }}
            className="flex gap-x-4 justify-center items-center pt-3 cursor-pointer"
          >
            <img src={logo} alt="logo" width={150} />
          </div>

          <ul className="pt-10">
            {Menu.map((item, index) => (
              <li key={index}>
                <div
                  onClick={() => onClickNavbar(index)}
                  className={`text-gray-300 text-sm flex ${
                    open ? "justify-start" : "justify-center"
                  } flex flex-1 items-center gap-x-4 cursor-pointer p-3 mx-3 hover:bg-light-white duration-300 rounded-md`}
                >
                  {item.icon}{" "}
                  <span
                    className={`${!open && "hidden"} origin-left duration-200`}
                  >
                    {item.title}
                  </span>
                </div>
              </li>
            ))}
          </ul>
        </div>
      )}
    </>
  );
};

export default observer(Navbar);

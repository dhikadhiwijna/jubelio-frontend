import jwtDecode from "jwt-decode";
import React from "react";
import { FaUser } from "react-icons/fa";

const CreateProduct = () => {
  //DECLARE STATE BY USING TOKEN
  const token = localStorage.getItem("token");
  const decode = jwtDecode(token);
  const { name, email } = decode;

  return (
    <div>
      <div className="w-screen h-screen" />
      <div
        className="absolute inset-0 overflow-y-auto flex justify-center items-center z-1 bg-black bg-opacity-50"
        onClick={() => {}}
      >
        <div className="max-w-sm rounded-lg border  shadow-md bg-dark-purple border-gray-700 px-20 ">
          <div className="flex flex-col items-center pb-10">
            <div className="flex justify-center items-center p-5 rounded-full bg-white m-5">
              <FaUser className="w-10 h-10 m-2 text-dark-purple" />
            </div>
            <h5 class="mb-1 text-2xl font-medium text-gray-900 dark:text-white">
              {name}
            </h5>
            <h5 class="mb-1 text-md font-medium text-gray-900 dark:text-white">
              {email}
            </h5>
            <span class="text-sm text-gray-500 dark:text-gray-400">Tester</span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CreateProduct;

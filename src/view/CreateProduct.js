import React from "react";
import { useNavigate } from "react-router";
import { URL } from "../config/url";
import { getWithExpiry } from "../helper/tokenExpire";

const CreateProduct = () => {
  //DECLARE STATE
  const [name, setName] = React.useState("");
  const [sku, setSku] = React.useState("");
  const [image, setImage] = React.useState([]);
  const [price, setPrice] = React.useState("");
  const [description, setDescription] = React.useState("");
  const navigate = useNavigate();

  //HANDLE SUBMIT
  const onClickSubmit = async () => {
    try {
      const formData = new FormData();
      formData.append("name", name);
      formData.append("sku", sku);
      formData.append("price", price);
      formData.append("description", description);
      formData.append("file", image);

      const response = await fetch(`${URL}/products/create`, {
        method: "POST",
        headers: {
          token: getWithExpiry("token"),
        },
        body: formData,
      });
      const result = await response.json();
      if (result) {
        navigate("../product");
      }
      console.log(result);
    } catch (error) {
      console.log("Error catch onSubmit: ", error);
    }
  };

  return (
    <div>
      <div className="w-screen h-screen" />
      <div
        className="fixed inset-0 overflow-y-auto flex justify-center items-center z-50 bg-black bg-opacity-50"
        onClick={() => {}}
      >
        <div className="absolute bg-white p-4 rounded-lg shadow-lg">
          <h1 className="block uppercase tracking-wide text-gray-700 text-lg font-bold my-2">
            Create Product
          </h1>
          <form className="w-full max-w-lg">
            <div className="flex flex-wrap -mx-3 mb-6">
              <div className="w-full px-3">
                <label
                  className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                  htmlFor="grid-name"
                >
                  NAME
                </label>
                <input
                  className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                  id="grid-name"
                  type="text"
                  placeholder="Enter Product Name"
                  value={name}
                  onChange={(e) => {
                    setName(e.target.value);
                  }}
                />
                <p className="text-gray-600 text-xs italic">
                  Required. 100 characters or less.
                </p>
              </div>
            </div>
            <div className="flex flex-wrap -mx-3 mb-6">
              <div className="w-full md:w-1/2 px-3 mb-6 md:mb-0">
                <label
                  className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                  htmlFor="grid-price"
                >
                  PRICE
                </label>
                <input
                  className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                  id="grid-price"
                  type="number"
                  placeholder="100000"
                  value={price}
                  onChange={(e) => setPrice(e.target.value)}
                />
              </div>
              <div className="w-full md:w-1/2 px-3">
                <label
                  className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                  htmlFor="grid-SKU"
                >
                  SKU
                </label>
                <input
                  className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                  id="grid-SKU"
                  type="text"
                  placeholder="Stock Keeping Unit"
                  value={sku}
                  onChange={(e) => setSku(e.target.value)}
                />
              </div>
            </div>
            <div className="flex flex-wrap -mx-3 mb-6">
              <div className="w-full px-3">
                <label
                  className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2"
                  htmlFor="grid-text"
                >
                  Description
                </label>
                <input
                  className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                  id="grid-text"
                  type="text"
                  placeholder="description"
                  value={description}
                  onChange={(e) => setDescription(e.target.value)}
                />
                <p className="text-gray-600 text-xs italic">
                  Not Required. Max of 150 characters.
                </p>
              </div>
            </div>
            <div className="flex flex-wrap -mx-3 mb-6">
              <div className="w-full px-3">
                <label className="block uppercase tracking-wide text-gray-700 text-xs font-bold mb-2">
                  File
                </label>

                <input
                  className="appearance-none block w-full bg-gray-200 text-gray-700 border border-gray-200 rounded py-3 px-4 mb-3 leading-tight focus:outline-none focus:bg-white focus:border-gray-500"
                  type="file"
                  accept="image/*"
                  onChange={(e) => setImage(e.target.files[0])}
                />

                <p className="text-gray-600 text-xs italic">
                  Required and important. Failed if not uploaded.
                </p>
              </div>
            </div>
          </form>
          <button
            onClick={() => onClickSubmit()}
            className="bg-dark-purple text-sm cursor-pointer px-3 py-2 hover:bg-gray-300 hover:text-dark-purple text-white duration-300 rounded-md"
          >
            SUBMIT
          </button>
          <button
            onClick={() => navigate("../product")}
            className="text-sm cursor-pointer px-3 py-2 ml-3  hover:bg-gray-300 hover:text-dark-purple text-white duration-300 rounded-md bg-yellow-400"
          >
            CANCEL
          </button>
        </div>
      </div>
    </div>
  );
};

export default CreateProduct;

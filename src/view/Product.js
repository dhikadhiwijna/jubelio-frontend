import React, { useCallback, useEffect, useRef, useState } from "react";
import { Outlet, useNavigate } from "react-router";
import { URL } from "../config/url";
import { getWithExpiry } from "../helper/tokenExpire";
import noimage from "../assets/noimage.webp";
import EditModal from "../component/EditModal";
import { observer } from "mobx-react";
import { FaPlus } from "react-icons/fa";

const Products = () => {
  //DECLARE STATE
  const limit = 12;
  const [offset, setOffset] = useState(0);
  const [dataProduct, setDataProduct] = useState([]);
  const tokenExpire = getWithExpiry("token");
  const navigate = useNavigate();
  const [openEditModal, setOpenEditModal] = useState(false);

  useEffect(() => {
    if (tokenExpire === null) {
      navigate("/");
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tokenExpire, offset]);

  //PAGINATION
  const [page, setPage] = useState({
    currentPage: 1,
    totalPage: 1,
  });
  const [loading, setLoading] = useState(false);

  //ON CLICK CARD PRODUCT
  const onClickCard = (index) => {
    navigate(`/product/${dataProduct[index].id}`);
  };

  //FETCH DATA PRODUCT
  const fetchData = async () => {
    try {
      setLoading(true);
      let response = await fetch(
        `${URL}/products?limit=${limit}&offset=${offset}`,
        {
          method: "GET",
          headers: {
            token: getWithExpiry("token"),
          },
        }
      );

      let result = await response.json();
      setDataProduct([...dataProduct, ...result.data]);
      setPage({
        currentPage: result.current_page,
        totalPage: result.total_page,
      });

      setLoading(false);
    } catch (error) {
      console.log("Error catch onSubmit: ", error);
    }
  };

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [offset]);

  //CREATE INFINITY SCROLL PAGINATION
  const observer = useRef();
  const lastProductElement = useCallback(
    (node) => {
      if (loading) return;
      if (observer.current) observer.current.disconnect();

      observer.current = new IntersectionObserver((entries) => {
        if (entries[0].isIntersecting) {
          console.log("VISIBLE");
          setOffset(offset + limit);
        }
      });
      if (node) observer.current.observe(node);
    },
    [observer, loading, offset, limit]
  );

  return (
    <div className="flex flex-1 flex-col bg-white rounded-lg bg-opacity-70 m-5 p-3 justify-center items-center text-center md:items-start md:text-left">
      <div className="mx-3">
        <h1 className="my-5 font-bold text-3xl">PRODUCT</h1>
        <button
          className=" w-60 mb-5 flex justify-center items-center  text-white focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center bg-dark-purple hover:bg-slate-400 duration-300 shadow-md"
          type="button"
          onClick={() => navigate("/product/create")}
        >
          <FaPlus className="mr-5" />
          <span>Add Products</span>
        </button>
      </div>

      <div className="flex flex-wrap justify-center">
        {dataProduct.map((item, index) => {
          if (dataProduct.length === index + 1) {
            return (
              <div
                ref={lastProductElement}
                key={index}
                className="rounded-lg shadow-md bg-dark-purple border-dark-purple max-w-xs m-3 w-52 cursor-pointer shadow-slate-700"
                onClick={() => onClickCard(index)}
              >
                <div className="h-40 overflow-hidden mb-4">
                  {item.image ? (
                    <img
                      className="rounded-t-lg w-full h-40 object-cover"
                      src={item.image}
                      alt={item.name}
                    />
                  ) : (
                    <img
                      className="rounded-t-lg w-full"
                      src={noimage}
                      alt="no_image"
                    />
                  )}
                </div>

                <div className="px-5 pb-3">
                  <h5 className="text-sm font-semibold tracking-tight text-gray-900 dark:text-white">
                    {item.name}
                  </h5>

                  <div className="flex justify-between items-center">
                    <span className="text-lg font-bold text-gray-900 dark:text-white">
                      Rp {item.price}
                    </span>
                  </div>
                </div>
              </div>
            );
          } else {
            return (
              <div
                key={index}
                className="rounded-lg shadow-md bg-dark-purple border-dark-purple max-w-xs m-3 w-52 cursor-pointer shadow-slate-700 hover:bg-slate-500 duration-300"
                onClick={() => onClickCard(index)}
              >
                <div className="h-40 overflow-hidden mb-4">
                  {item.image ? (
                    <img
                      className="rounded-t-lg w-full h-40 object-cover"
                      src={item.image}
                      alt={item.name}
                    />
                  ) : (
                    <img
                      className="rounded-t-lg w-full"
                      src={noimage}
                      alt="no_image"
                    />
                  )}
                </div>

                <div className="px-5 pb-3">
                  <h5 className="text-sm font-semibold tracking-tight text-gray-900 dark:text-white">
                    {item.name}
                  </h5>

                  <div className="flex justify-between items-center">
                    <span className="text-lg font-bold text-gray-900 dark:text-white">
                      Rp {item.price}
                    </span>
                  </div>
                </div>
              </div>
            );
          }
        })}
      </div>
      {loading && page.currentPage <= page.totalPage && (
        <div className="text-center text-xl font-bold text-gray-900 justify-center items-center hover:bg-slate-500 duration-300">
          Loading...
        </div>
      )}
      <EditModal shown={openEditModal} close={() => setOpenEditModal(false)} />
      <Outlet />
    </div>
  );
};

export default observer(Products);

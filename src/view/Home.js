import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";
import welcome from "../assets/welcome.svg";
import { AuthData } from "../store/auth.store";
import { observer } from "mobx-react";
import jwtDecode from "jwt-decode";
import { getWithExpiry } from "../helper/tokenExpire";
const Home = () => {
  //DECLARE STATE
  const authenticated = AuthData.isAuthenticated();
  const token = getWithExpiry("token");
  const [data, setData] = useState(null);

  //CHECK TOKEN
  useEffect(() => {
    if (token) {
      setData(jwtDecode(token));
    }
  }, [token]);

  return (
    <div className="p-7 flex flex-1 flex-col h-screen items-center justify-center">
      <div className="my-5 shadow-lg bg-white p-2 rounded-md flex flex-col">
        <div className="flex justify-center items-center pt-2 px-10 flex-col md:flex-row">
          {authenticated ? (
            <div className="flex flex-col gap-y-4 text-center md:text-left">
              <h1 className="text-3xl font-bold uppercase text-dark-purple md:text-5xl">
                Welcome {data?.name}
              </h1>
              <h1 className="text-lg md:text-xl">
                You can check our products and services here.
              </h1>
            </div>
          ) : (
            <div className="flex flex-col gap-y-4 text-center md:text-left">
              <h1 className="text-3xl font-bold uppercase text-dark-purple md:text-5xl">
                Welcome to Jubelio
              </h1>
              <h1 className="text-lg md:text-xl">
                Please sign in or sign up to see all available content
              </h1>
              <div className="flex text-sm justify-center md:justify-start">
                <div>
                  <Link to="./signin">
                    <button className="bg-dark-purple gap-x-4 font-bold cursor-pointer px-3 py-2 hover:bg-gray-300 hover:text-dark-purple text-white duration-300 rounded-md">
                      LOGIN
                    </button>
                  </Link>
                  <Link to="./signup">
                    <button className="bg-dark-purple gap-x-4 font-bold cursor-pointer px-3 py-2 mx-3 hover:bg-gray-300 hover:text-dark-purple text-white duration-300 rounded-md">
                      REGISTER
                    </button>
                  </Link>
                </div>
              </div>
            </div>
          )}

          <img src={welcome} alt="Welcome" className="h-1/2 w-1/2 p-5" />
        </div>
      </div>
    </div>
  );
};

export default observer(Home);

import jwtDecode from "jwt-decode";
import React, { useState } from "react";
import { FaUser } from "react-icons/fa";
import { useNavigate } from "react-router";
import { URL } from "../config/url";
import { AuthData } from "../store/auth.store";
import { setWithExpiry } from "../helper/tokenExpire";

const SignUp = () => {
  //DECLARE VARIABLE
  const navigate = useNavigate();
  const [name, setName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  //DECLARE FUNCTION FOR SUBMIT DATA
  const onSubmit = async (e) => {
    e.preventDefault();
    try {
      let body = JSON.stringify({ name, email, password });

      let response = await fetch(`${URL}/auth/signup`, {
        method: "POST",
        body: body,
        headers: {
          "Content-Type": "Application/json",
        },
      });
      let result = await response.json();
      console.log(result);

      if (result.statusCode === 200) {
        let decode = await jwtDecode(result.token);
        AuthData.changeAuthenticationTrue();
        setName("");
        setEmail("");
        setPassword("");
        setWithExpiry("token", result.token, decode.exp);
        navigate("../");
      }
    } catch (error) {
      console.log("Error catch onSubmit: ", error);
    }
  };

  return (
    <div className="p-7 flex flex-1 flex-col h-screen items-center justify-center text-white bg-black bg-opacity-30">
      <form
        onSubmit={(e) => onSubmit(e)}
        className="bg-dark-purple w-96 p-4 rounded-md flex flex-col justify-start gap-y-4"
      >
        <div className="flex items-center gap-x-3">
          <FaUser />
          <h1 className="my-4 font-bold uppercase">Register</h1>
        </div>
        <input
          type="text"
          placeholder="Name"
          value={name}
          onChange={(e) => setName(e.target.value)}
          className="bg-white p-2 rounded-sm text-dark-purple hover:border-dark-purple focus:outline-none"
        />
        <input
          type="email"
          placeholder="Email"
          value={email}
          onChange={(e) => setEmail(e.target.value)}
          className="bg-white p-2 rounded-sm text-dark-purple hover:border-dark-purple focus:outline-none"
        />
        <input
          type="password"
          placeholder="Password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
          className="bg-white p-2 rounded-sm text-dark-purple hover:border-dark-purple focus:outline-none"
        />
        <button
          type="submit"
          className=" uppercase bg-dark-purple gap-x-4 cursor-pointer px-3 py-2 hover:bg-gray-300 hover:text-dark-purple text-white duration-300 rounded-md "
        >
          Sign Up
        </button>
      </form>
    </div>
  );
};

export default SignUp;

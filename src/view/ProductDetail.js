import React, { useEffect, useState } from "react";
import { productStore } from "../store/product.store";
import noimage from "../assets/noimage.webp";
import { getWithExpiry } from "../helper/tokenExpire";
import { URL } from "../config/url";
import EditModal from "../component/EditModal";
import { useLocation } from "react-router";
import DeleteModal from "../component/DeleteModal";

export default function ProductDetail() {
  //DECLARE STATE
  const location = useLocation();
  const path = location.pathname.split("/")[2];
  const [detail, setDetail] = useState({
    name: "",
    price: "",
    description: "",
    image: noimage,
    category: "",
    sku: "",
  });

  //DECLARE STATE FOR EDIT & DELETE MODAL
  const [openModalEdit, setModalEdit] = useState(false);
  const [openModalDelete, setModalDelete] = useState(false);

  //DECLARE FUNCTION FOR GETTING DETAIL PRODUCT
  const fetchData = async () => {
    try {
      let response = await fetch(`${URL}/products/detail/${path}`, {
        method: "GET",
        headers: {
          token: getWithExpiry("token"),
        },
      });

      let result = await response.json();
      setDetail(result[0]);
      productStore.setDataProductStore(result[0]);
    } catch (error) {
      console.log("Error catch onSubmit: ", error);
    }
  };

  useEffect(() => {
    fetchData();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <>
      <div className="container px-5 py-24 bg-white md:ml-5 duration-300">
        <div className="lg:w-4/5 mx-auto flex flex-wrap">
          <img
            alt={detail.name}
            className="lg:w-1/2 w-full lg:h-auto h-64 object-cover object-center rounded"
            src={detail.image ?? noimage}
          />
          <div className="lg:w-1/2 w-full lg:pl-10 lg:py-6 mt-6 lg:mt-0">
            <h2 className="text-sm title-font text-gray-500 tracking-widest">
              PRODUCT DETAIL
            </h2>
            <h1 className="text-gray-900 text-3xl title-font font-medium mb-1">
              {detail.name}
            </h1>
            <div className="flex mb-4"></div>
            <p className="leading-relaxed">{detail.description}</p>
            <p className="leading-relaxed">{detail.sku}</p>

            <div className="flex justify-between">
              <span className="title-font font-medium text-2xl text-gray-900">
                Rp {detail.price}
              </span>

              <div className="flex">
                <button
                  onClick={() => setModalEdit(true)}
                  className="flex ml-auto text-white bg-dark-purple border-0 py-2 px-6 focus:outline-none hover:bg-slate-400 duration-300 rounded"
                >
                  Edit
                </button>
                <button
                  onClick={() => setModalDelete(true)}
                  className="flex ml-5 text-white bg-red-500 border-0 py-2 px-6 focus:outline-none hover:bg-red-100 duration-300 rounded"
                >
                  Delete
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>

      <EditModal shown={openModalEdit} close={() => setModalEdit(false)} />
      <DeleteModal
        shown={openModalDelete}
        close={() => setModalDelete(false)}
        id={path}
      />
    </>
  );
}

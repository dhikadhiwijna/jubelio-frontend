import { AuthData } from "../store/auth.store";

export const getWithExpiry = (key) => {
  const itemStr = localStorage.getItem(key);
  // IF TOKEN NOT EXISTS
  if (!itemStr) {
    AuthData.changeAuthenticationFalse();
    return null;
  }
  const item = JSON.parse(itemStr);
  const now = new Date();
  // COMPARE EXPIRY TIME
  if (now.getTime() > item.expiry) {
    // IF THE ITEM EXPIRED, REMOVE IT
    AuthData.changeAuthenticationFalse();
    localStorage.removeItem(key);
    return null;
  }
  return item.value;
};

export const setWithExpiry = (key, value, ttl) => {
  const now = new Date();

  // SET EXPIRY TIME, IN MILLISECONDS
  const item = {
    value: value,
    expiry: now.getTime() + ttl,
  };
  AuthData.changeAuthenticationTrue();
  localStorage.setItem(key, JSON.stringify(item));
};
